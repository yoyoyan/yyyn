/******************************
	* 二元連立微分方程式のRungeKutta法による数値解析。$(BR)
	* 	dx/dt = f(t,x,y)$(BR)
	* 	dy/dt = g(t,x,y)$(BR)
	*
	* Authors: yoyoyan
	*/
module yyyn.calc.runge_kutta;

/******************************
	* 初期値(x0,y0)から、$(BR)
	* 二元連立一階微分方程式$(BR)
	* dx/dt = f(t,x,y)$(BR)
	* dy/dt = g(t,x,y)$(BR)
	* をRunge-Kutta法によって求める。$(BR)
	*
	* Params:
	* 	x0 = xの初期値
	* 	y0 = yの初期値
	* 	delegate(real,real,real) f = 上式のf(t,x,y)
	* 	delegate(real,real,real) g = 上式のg(t,x,y)
	* 	dt = tのステップ
	* 	pointNum = 出力される点の数
	*
	* Returns:
	* 	点を格納した配列real[]をx,y分出力する
	*/
real[][2] rungeKutta(
		real x0, real y0,
		real delegate(real, real, real) f,
		real delegate(real, real, real) g,
		real dt, uint pointNum
		) {

	real t = 0.0;
	real k0,k1,k2,k3, l0,l1,l2,l3;		//k:xの増分 l:yの増分

	real[][2] points;
	points[0] = new real[pointNum];
	points[1] = new real[pointNum];
	real[] x = points[0];
	real[] y = points[1];
	x[0] = x0, y[0] = y0;

	for(uint i = 1; i < pointNum; i++) {
		k0 = dt * f(t, x[i-1], y[i-1]);
		l0 = dt * g(t, x[i-1], y[i-1]);

		k1 = dt * f(t + dt/2, x[i-1] + k0/2, y[i-1] + l0/2);
		l1 = dt * g(t + dt/2, x[i-1] + k0/2, y[i-1] + l0/2);

		k2 = dt * f(t + dt/2, x[i-1] + k1/2, y[i-1] + l1/2);
		l2 = dt * g(t + dt/2, x[i-1] + k1/2, y[i-1] + l1/2);

		k3 = dt * f(t + dt, x[i-1] + k2, y[i-1] + l2);
		l3 = dt * g(t + dt, x[i-1] + k2, y[i-1] + l2);

		k0 = (k0 + 2*k1 + 2*k2 + k3) / 6;
		l0 = (l0 + 2*l1 + 2*l2 + l3) / 6;

		t += dt;
		x[i] = x[i-1] + k0;
		y[i] = y[i-1] + l0;
	}

	return points;
}



///
class RungeKutta {
	private {
		real t;
		real dt;
		real[4] k,l;
		real[2] prevPoint;
		real delegate(real, real, real)[2] f;
	}

	/******************************
		* コンストラクタ。$(I init())を呼ぶ。
		* $(I init()) と同様
		*/
	this(real[2] x0, real delegate(real, real, real)[2] f, real dt) {
		init(x0, f, dt);
	}

	/******************************
		* 適用する式の初期化
		* Params:
		* 	x0 = 初期値
		* 	f = 適用する式
		* 	dt = tのステップ
		*/
	void init(
			real[2] x0,
			real delegate(real, real, real)[2] f,
			real dt
			) {
		t = 0;
		this.prevPoint = x0;
		this.f = f;
		this. dt = dt;
	}

	/******************************
		* 次の点を求める。
		*
		* Returns:
		* 	次の点
		*/
	real[2] popFront() {
		k[0] = dt * f[0](t, prevPoint[0], prevPoint[1]);
		l[0] = dt * f[1](t, prevPoint[0], prevPoint[1]);

		k[1] = dt * f[0](t + dt/2, prevPoint[0] + k[0]/2, prevPoint[1] + l[0]/2);
		l[1] = dt * f[1](t + dt/2, prevPoint[0] + k[0]/2, prevPoint[1] + l[0]/2);

		k[2] = dt * f[0](t + dt/2, prevPoint[0] + k[1]/2, prevPoint[1] + l[1]/2);
		l[2] = dt * f[1](t + dt/2, prevPoint[0] + k[1]/2, prevPoint[1] + l[1]/2);

		k[3] = dt * f[0](t + dt, prevPoint[0] + k[2], prevPoint[1] + l[2]);
		l[3] = dt * f[1](t + dt, prevPoint[0] + k[2], prevPoint[1] + l[2]);

		k[0] = (k[0] + 2*k[1] + 2*k[2] + k[3]) / 6;
		l[0] = (l[0] + 2*l[1] + 2*l[2] + l[3]) / 6;

		t += dt;
		prevPoint[0] += k[0];
		prevPoint[1] += l[0];

		return prevPoint;
	}
}

